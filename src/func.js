const getSum = (str1, str2) => {
  if (typeof str1 != "string" || typeof str2 != "string") {
    return false;
  }
  if (isNaN(Number(str1)) || isNaN(Number(str2))) {
    return false;
  }
  return String(Number(str1)+Number(str2));
};

const getQuantityPostsByAuthor = (listOfPosts, authorName) => {
  let Posts = 0;
    let Coments = 0;
    for (const element of listOfPosts) {
      if (element.author == authorName) {
        Posts++;  
      }
      if (element.hasOwnProperty("comments")) {
        for (let index2 = 0; index2 < element.comments.length; index2++) {
            if (element.comments[index2].author == authorName) {
                Coments++;
            }
        }
      }
    }
    return `Post:${Posts},comments:${Coments}`;
};

const tickets=(people)=> {
  let tisketPrice = 25;
  let clerk = 0;
  for (const element of people) {
    if (element==tisketPrice) {
      clerk += element;
    }
    if ((element-tisketPrice) <= clerk) {
      clerk += -(element-tisketPrice);
      clerk += tisketPrice;
    } else {
      return "NO";
    }
    
  }
  return "YES";
};


module.exports = {getSum, getQuantityPostsByAuthor, tickets};
